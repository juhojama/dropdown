var dropdownClassList, dropdownClassListLength, selectElemLength, selectElem, selectedItem, selectItems, newSelectElem;

/* Look for any elements with the class 'dropdown' */
dropdownClassList = document.getElementsByClassName('dropdown');
dropdownClassListLength = dropdownClassList.length;

for (var i = 0; i < dropdownClassListLength; i++) {
    selectElem = dropdownClassList[i].getElementsByTagName('select')[0];
    selectElemLength = selectElem.length;

    /* For each element, create a new div that will act as the selected item */
    selectedItem = document.createElement('div');
    selectedItem.setAttribute('class', 'select-selected');
    selectedItem.innerHTML = selectElem.options[selectElem.selectedIndex].innerHTML;
    dropdownClassList[i].appendChild(selectedItem);

    /* For each element, create a new div that will contain the option list */
    selectItems = document.createElement('div');
    selectItems.setAttribute('class', 'select-items select-hide');

    for (var j = 0; j < selectElemLength; j++) {
        /* For each option in the original select element,
        create a new div that will act as an option item */

        newSelectElem = document.createElement('div');
        newSelectElem.innerHTML = selectElem.options[j].innerHTML;

        newSelectElem.addEventListener('click', function (e) {

            /* When an item is clicked, update the original select box,
            and the selected item */
            var y, i, k, s, h, sLength, yLength;
            s = this.parentNode.parentNode.getElementsByTagName('select')[0];
            sLength = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sLength; i++) {
                if (s.options[i].innerHTML === this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName('same-as-selected');
                    yLength = y.length;
                    for (k = 0; k < yLength; k++) {
                        y[k].removeAttribute('class');
                    }
                    this.setAttribute('class', 'same-as-selected');
                    break;
                }
            }
            h.click();
        });

        selectItems.appendChild(newSelectElem);
    }

    dropdownClassList[i].appendChild(selectItems);

    dropdownClassList[i].addEventListener('click', function (e) {
        /* When the select box is clicked, close any other select boxes,
        and open/close the current select box */
        e.stopPropagation();

        var thisSelectedItem = document.getElementsByClassName('select-selected')[0];

        closeAllSelects(thisSelectedItem);
        thisSelectedItem.nextSibling.classList.toggle('select-hide');
        this.classList.toggle('active');
    });
}

function closeAllSelects(elem) {

    /* Will close all select boxes in the document,
    except the current select box */
    var y, i, xl, yl, arrNo = [];
    dropdownClassList = document.getElementsByClassName('select-items');
    y = document.getElementsByClassName('select-selected');
    xl = dropdownClassList.length;
    yl = y.length;

    for (i = 0; i < yl; i++) {
        if (elem === y[i]) {
            arrNo.push(i)
        } else {
            y[i].parentElement.classList.remove('active');
        }
    }

    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
            dropdownClassList[i].classList.add('select-hide');
        }
    }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes */
document.addEventListener('click', closeAllSelects);
